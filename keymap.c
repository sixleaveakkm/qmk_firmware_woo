# include QMK_KEYBOARD_H

extern keymap_config_t keymap_config;

#define _QWERTY 0
#define _MOVE 1
#define _RAISE 2
#define _MOUSE 3
#define _NUMS 4
#define _DOTA 5
enum custom_keycodes {
    PASSWD = SAFE_RANGE,
};

#define EISU LALT(KC_GRV)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
   /* Qwerty
   * ,----------------------------------------------------------------------------------------------------------------------.
   * | ESC  |   1  |   2  |   3  |   4  |   5  |   6  |                    |   7  |   8  |   9  |   0  |   -  |  +   | Bksp |
   * |------+------+------+------+------+------+------+--------------------+------+------+------+------+------+------+------|
   * | Tab  |   Q  |   W  |   E  |   R  |   T  | xxxx |                    |   Y  |   U  |   I  |   O  |   P  |   [  |  \   |
   * |------+------+------+------+------+------+------+--------------------+------+------+------+------+------+------+------|
   * | CapLk|   A  |   S  |   D  |   F  |   G  |A(Sft)|                    |   H  |   J  |   K  |   L  |   ;  |  "   | Enter|
   * |------+------+------+------+------+------+---------------------------+------+------+------+------+------+------+------|
   * | Shift|   Z  |   X  |   C  |   V  |   B  | NUMS |                    | XXXX |   N  |   M  |   ,  |   .  |   /  | RSh  |
   * |-------------+------+------+------+------+------+------+------+------+------+------+------+------+------+-------------|
   * | Ctrl |  GUI |  ALt | EISU |||||||| TO1 | Space| TO3   |||||||| Enter| Space| Raise|||||||| KANA |      |  ]   | RCtrl|
   * ,----------------------------------------------------------------------------------------------------------------------.
   */
  [_QWERTY] = LAYOUT( \
    KC_ESC,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,                           KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,  KC_BSPC, \
    KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_NO,                          KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC, KC_BSLS, \
    KC_CLCK, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    A(KC_LSFT),                     KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT, KC_ENT,  \
    KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    TG(_NUMS),                      KC_NO ,  KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT, \
    KC_LCTL, KC_LGUI, KC_LALT, LGUI_T(EISU),     LT(_MOVE,KC_NO),    KC_SPC
	,LT(_MOUSE,KC_NO),         KC_ENT,  KC_SPC , LT(_RAISE,KC_NO),       KC_KANA, KC_NO,   KC_RBRC, KC_RGHT  \
  ),

  /* MOVE
   * ,----------------------------------------------------------------------------------------------------------------------.
   * |      |      |      |      |      |      |      |                    |      |      |      |      |      |      |      |
   * |------+------+------+------+------+------+------+--------------------+------+------+------+------+------+------+------|
   * |      |      |      |      |      |      |      |                    |      |      | UP   |      |      | PGUP | PGDWN|
   * |------+------+------+------+------+------+------+--------------------+------+------+------+------+------+------+------|
   * |      |      |      |  DEL |      |      | xxxx |                    |      | HOME | LEFT | RIGHT| END  | Bksp |      |
   * |------+------+------+------+------+------+---------------------------+------+------+------+------+------+------+------|
   * |      |      |      |      |      |      |      |                    |      | DOWN | DOWN |      |      |      |      |
   * |-------------+------+------+------+------+------+------+------+------+------+------+------+------+------+-------------|
   * |      |      |      |      |||||||| TRANS|      |      ||||||||      |      |      ||||||||      |      |      |      
   * ,----------------------------------------------------------------------------------------------------------------------.
   */
  [_MOVE] = LAYOUT( \
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, \
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                        XXXXXXX, XXXXXXX, KC_UP,   XXXXXXX, XXXXXXX, KC_PGUP, KC_PGDN, \
    XXXXXXX, XXXXXXX, XXXXXXX, KC_DEL,  XXXXXXX, XXXXXXX, XXXXXXX,                        XXXXXXX, KC_HOME, KC_LEFT, KC_RIGHT,KC_END,  KC_BSPC, XXXXXXX, \
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                        XXXXXXX, KC_DOWN, KC_DOWN, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, \
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,          KC_TRNS, XXXXXXX, XXXXXXX,      XXXXXXX, XXXXXXX, XXXXXXX,          XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX  \
  ),

    /* RAISE
   * ,----------------------------------------------------------------------------------------------------------------------.
   * |  ~   |  F1  | F2   | F3   | F4   | F5   | F6   |                    | F7   | F8   | F9   | F10  | F11  | F12  | Del  |
   * |------+------+------+------+------+------+------+--------------------+------+------+------+------+------+------+------|
   * |      | PrvT | Start| NxtT |      |      | Paus |                    |      | Mute | Vol- | Vol+ |      |      |      |
   * |------+------+------+------+------+------+------+--------------------+------+------+------+------+------+------+------|
   * |      | Pass | F13  | F14  | F15  | F16  | Prsc |                    |      |MacMuT|MacVo-|MacVo+|      |      |SWESC |
   * |------+------+------+------+------+------+---------------------------+------+------+------+------+------+------+------|
   * |      | App  | F17  | F18  | F19  | F20  | F21  |                    |      |      |      |      |      |      |UNSWE |
   * |-------------+------+------+------+------+------+------+------+------+------+------+------+------+------+-------------|
   * |      | MENU |      |      ||||||||      |      |      ||||||||      |      | TRANS||||||||TG(DT)|      |TG(0) |      |
   * ,----------------------------------------------------------------------------------------------------------------------.
   */
  [_RAISE] = LAYOUT( \
    KC_GRV , KC_F1  , KC_F2  , KC_F3  , KC_F4  , KC_F5  , KC_F6  ,                        KC_F7  , KC_F8  , KC_F9      , KC_F10   , KC_F11 , KC_F12 , KC_DEL, \
    XXXXXXX, KC_MPRV, KC_MPLY, KC_MNXT, XXXXXXX, XXXXXXX, KC_PAUS,                        XXXXXXX, KC_MUTE, KC_VOLD    , KC_VOLU  , XXXXXXX, XXXXXXX, XXXXXXX, \
    XXXXXXX, PASSWD, KC_F13 , KC_F14 , KC_F15 , KC_F16 , KC_PSCR,                         XXXXXXX,KC__MUTE, KC__VOLDOWN, KC__VOLUP, XXXXXXX, XXXXXXX, MAGIC_SWAP_GRAVE_ESC, \
    XXXXXXX, KC_APP , KC_F17 , KC_F18 , KC_F19 , KC_F20 , KC_F21 ,                        XXXXXXX, XXXXXXX, XXXXXXX    , XXXXXXX  , XXXXXXX, XXXXXXX, MAGIC_UNSWAP_GRAVE_ESC, \
    XXXXXXX, KC_MENU, XXXXXXX, XXXXXXX,          XXXXXXX, XXXXXXX, XXXXXXX,      XXXXXXX, KC_TRNS, XXXXXXX,              TG(_DOTA), XXXXXXX,TG(_QWERTY), XXXXXXX  \
  ),

  /* MOUSE
   * ,----------------------------------------------------------------------------------------------------------------------.
   * |      |      |      |      |      |      |      |                    |      |      |      |      |      |      |      |
   * |------+------+------+------+------+------+------+--------------------+------+------+------+------+------+------+------|
   * |      |      |      |      |      |      |      |                    |      |      |MSUP  |      |      |MSWUP |MSWDWN|
   * |------+------+------+------+------+------+------+--------------------+------+------+------+------+------+------+------|
   * |      |      |      |      |      |      |      |                    |      |MSWLFT|MSLFT |MSRGT |MSWRGT|      |      |
   * |------+------+------+------+------+------+---------------------------+------+------+------+------+------+------+------|
   * |      |      |      |      |      |      |      |                    |      |MSDWN |MSDWN |      |      |      |      |
   * |-------------+------+------+------+------+------+------+------+------+------+------+------+------+------+-------------|
   * |      |      |      |      ||||||||      |      | TRANS||||||||KCBTN1|KCBTN2|KCBTN3||||||||KCACL0|KCACL1|KCACL2|      |
   * ,----------------------------------------------------------------------------------------------------------------------.
   */
  [_MOUSE] = LAYOUT( \
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                        XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, \
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
	XXXXXXX, XXXXXXX, KC_MS_U, XXXXXXX, XXXXXXX, KC_WH_U, KC_WH_D, \
    XXXXXXX, XXXXXXX, XXXXXXX, KC_DEL,  XXXXXXX, XXXXXXX, XXXXXXX,
	XXXXXXX, KC_WH_L, KC_MS_L, KC_MS_R, KC_WH_R, XXXXXXX, XXXXXXX, \
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                        XXXXXXX, KC_MS_D, KC_MS_D, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, \
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,          XXXXXXX, XXXXXXX, KC_TRNS,      KC_BTN1, KC_BTN2, KC_BTN3,          KC_ACL0, KC_ACL1, KC_ACL2, XXXXXXX  \
  ),

  /* NUMS
   * ,----------------------------------------------------------------------------------------------------------------------.
   * |      |      |      |      |      |      |      |                    |      | NUMLK| NUM/ | NUM* | NUM- |      |      |      
   * |------+------+------+------+------+------+------+--------------------+------+------+------+------+------+------+------|
   * |      |      |      |      |      |      |      |                    |      | NUM7 | NUM8 | NUM9 | NUM+ |      |      |
   * |------+------+------+------+------+------+------+--------------------+------+------+------+------+------+------+------|
   * |      |      |      |      |      |      |      |                    |      | NUM4 | NUM5 | NUM6 |NUM+  |      |      |
   * |------+------+------+------+------+------+---------------------------+------+------+------+------+------+------+------|
   * |      |      |      |      |      |      | TG(N)|                    |      | NUM1 | NUM2 | NUM3 |NUMENT|      |      |
   * |-------------+------+------+------+------+------+------+------+------+------+------+------+------+------+-------------|
   * |      |      |      |      ||||||||      |      |      ||||||||      |      | NUM0 ||||||||  NUM.|NUMENT|      |      |      
   * ,----------------------------------------------------------------------------------------------------------------------.
   */
  [_NUMS] = LAYOUT( \
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                        XXXXXXX, KC_NLCK, KC_PSLS, KC_PAST, KC_PMNS, XXXXXXX, XXXXXXX, \
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                        XXXXXXX, KC_P7,   KC_P8,   KC_P9  , KC_PPLS, XXXXXXX, XXXXXXX, \
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                        XXXXXXX, KC_P4,   KC_P5,   KC_P6  , KC_PPLS, XXXXXXX, XXXXXXX, \
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, TG(_NUMS),                      XXXXXXX, KC_P1,   KC_P2,   KC_P3  , KC_PENT, XXXXXXX, XXXXXXX, \
    XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,          XXXXXXX, XXXXXXX, XXXXXXX,      XXXXXXX, XXXXXXX, KC_P0,            KC_PDOT, KC_PENT, XXXXXXX, XXXXXXX  \
  ),

  /* DOTA
   * ,----------------------------------------------------------------------------------------------------------------------.
   * |  ~   |   1  |   2  |   3  |   4  |   5  |   6  |                    |   7  |   8  |   9  |   0  |   -  |  +   | Bksp |
   * |------+------+------+------+------+------+------+--------------------+------+------+------+------+------+------+------|
   * | Tab  |   Q  |   W  |   E  |   R  |   T  |   Y  |                    |   Y  |   U  |   I  |   O  |   P  |   [  |  \   |
   * |------+------+------+------+------+------+------+--------------------+------+------+------+------+------+------+------|
   * | CapLk|   A  |   S  |   D  |   F  |   G  |      |                    |   H  |   J  |   K  |   L  |   ;  |  "   | Enter|
   * |------+------+------+------+------+------+---------------------------+------+------+------+------+------+------+------|
   * | Shift|   Z  |   X  |   C  |   V  |   B  | NUMS |                    | XXXX |   N  |   M  |   ,  |   .  |   /  | RSh  |
   * |-------------+------+------+------+------+------+------+------+------+------+------+------+------+------+-------------|
   * | Ctrl |  GUI |  ALt | ALT  |||||||| MOVE | Space| F6   |||||||| Enter| Space| Raise|||||||| KANA |      |  ]   | RCtrl|
   * ,----------------------------------------------------------------------------------------------------------------------.
   */
  [_DOTA] = LAYOUT( \
    KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,                           KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,  KC_BSPC, \
    KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,                           KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC, KC_BSLS, \
    KC_CLCK, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    A(KC_LSFT),                     KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT, KC_ENT,  \
    KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    TG(_NUMS),                      KC_NO ,  KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT, \
    KC_LCTL, KC_LGUI, KC_LALT, KC_LALT, TO(_MOVE), KC_SPC ,KC_F6,
	KC_ENT,  KC_SPC , LT(_RAISE,KC_NO),       TG(_DOTA), XXXXXXX,TG(_QWERTY), XXXXXXX  \
  )
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case PASSWD:
      if (record->event.pressed) {
        SEND_STRING("");
      } else {

      }
      break;
  }
  return true;
}
